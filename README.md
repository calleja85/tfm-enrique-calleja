# TFM Enrique CALLEJA

Trabajo Fin de Máster: Estudio del efecto de los eventos en la cotización de las criptomonedas.

Estructura de carpetas:
- **data**: Contiene los datasets empleados en el estudio.
- **docs**: Contiene la memoria del TFM.
- **src**: Archivos de código, tanto en R como en Python, sobre los que se sustenta el estudio:
    - **"TFM_AnalisisEventos vEventstudyPython.ipynb"**: Es el archivo final en el cual se sustenta el estudio de eventos realizado. Utiliza la libreria *eventstudy* de Python.
    - **"CoinMarketCap_scraping.ipynb"**: Notebook en el que se obtiene la serie histórica de precios para las criptomonedas objeto de analisis, realizando scraping sobre el sitio Web CoinMarketCap.
    - **"TFM_AnalisisEventos vEventStudies.Rmd"**: Archivo en R que utiliza la libreria *eventstudies* para realizar dicho estudio. Se descarta el uso de esta libreria por su baja flexibilidad a la hora de modificar determinados parametros del estudio de eventos, sin embargo en este archivo es donde se realiza el análisis exploratorio y donde se da formato a alguno de los dataframes que se utilizan en el archivo de Python.
    - "TFM_AnalisisEventos.Rmd": Archivo con una línea de trabajo descartada por no ser posible la conclusión de los calculos. Utiliza la libreria *EventStudy* necesaria para utilizar el paquete de herramientas de Event Study Tools.